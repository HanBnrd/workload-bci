# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from socket import socket
import matplotlib.pyplot as plt


TCP_IP = '127.0.0.1'
TCP_PORT = 7777
BUFFER_SIZE = 500  # must be more than the size of a line
WINDOW_SIZE = 50  # window size to display


def main():
    # Notice
    print('Workload BCI')
    print('Copyright (C)  Johann Benerradi')
    print('This program comes with ABSOLUTELY NO WARRANTY.')
    print('This is free software, and you are welcome to')
    print('redistribute it under certain conditions.')
    print('---')

    # Connect to the port sending Oxysoft ASCII
    s = socket()
    s.connect((TCP_IP, TCP_PORT))

    # File to save the data
    csv = open('../data/data.csv', 'w', encoding='utf8')

    # Set for plotting
    plt.axis([0, WINDOW_SIZE, -100, 100])
    plt.autoscale(enable=True, axis='y')
    sclist = []

    # Handle headers
    data = s.recv(BUFFER_SIZE)  # recieve first buffer
    if data:
        # Format the data
        line = str(data)
        line = line.split('b\'')[1]
        line = line.split('\\n')[0]
        # Print in console
        print("Received data: " + str(line))
        # Write in the CSV file
        csv.write(line + '\n')

    # Skip messy lines
    for _ in range(20):
        data = s.recv(BUFFER_SIZE)

    while True:
        # Recieve one line
        data = s.recv(BUFFER_SIZE)

        # End the loop if nothing is recieved
        if not data:
            break

        # Format the data
        line = str(data)
        line = line.split('b\'')[1]
        line = line.split('\\n\'')[0]

        # Print in console
        print("Received data: " + str(line))

        # Write in the CSV file
        csv.write(line + '\n')

        # Get coordinates
        x = float(line.split(', ')[0])
        i = x % WINDOW_SIZE
        yOx = float(line.split(', ')[1])
        yDeOx = float(line.split(', ')[2])
        y = yOx-yDeOx

        # Remove the oldest point to refresh
        if len(sclist) > WINDOW_SIZE-2:
            sc = sclist.pop(0)
            sc.remove()

        # Display point
        sc = plt.scatter(i, y, c='red')
        sclist.append(sc)
        plt.pause(0.001)


if __name__ == '__main__':
    main()
