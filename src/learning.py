# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
import tkinter.font as tkf
from tkinter import ttk
from pathlib import Path
from random import shuffle

import lib as lib


def run(entry_frequency, entry_duration, condition_name, progressbar,
        baseline=60):
    """
    Export function
    """
    frequency = int(entry_frequency.get())
    duration = int(entry_duration.get())
    total_duration = baseline + duration
    thread_export = lib.Export(frequency, total_duration, condition_name)
    thread_launcher = lib.Launcher(condition_name, baseline)
    thread_pb = lib.Progressbar(duration, progressbar, baseline)
    thread_export.start()
    thread_launcher.start()
    thread_pb.start()


def process(conditions, root):
    """
    Processing function
    """
    pr = lib.Process()
    pr.get_extremums()
    for condition in conditions:
        pr.normalize(condition)
        pr.average(condition)
    root.destroy()


def main():
    # Notice
    print('Workload BCI')
    print('Copyright (C)  Johann Benerradi')
    print('This program comes with ABSOLUTELY NO WARRANTY.')
    print('This is free software, and you are welcome to')
    print('redistribute it under certain conditions.')
    print('---')

    # Print randomized task order
    conditions = ['high', 'medium', 'low']
    shuffle(conditions)
    print(conditions)

    # Set window
    root = tk.Tk()
    root.title('Workload BCI')
    root.configure(background='#292d34')

    # Define fonts
    title = tkf.Font(family='Courier', size=45, weight='bold')
    subtitle = tkf.Font(family='Courier', size=20)
    text = tkf.Font(family='Courier', size=12, weight='bold')

    # Title
    label = tk.Label(root, text='Workload BCI', padx=40, pady=3, font=title,
                     bg='#292d34', fg='#d9d9d9')
    label.pack()

    # Settings frame
    lf_settings = tk.LabelFrame(root, text='Settings', font=subtitle, bd=3,
                                padx=20, pady=15, bg='#292d34', fg='#d9d9d9')
    lf_settings.pack(fill='both', expand='yes')
    # Frequency entry
    l_frequency = tk.Label(lf_settings, text='Frequency (Hz) ', font=text,
                           bg='#292d34', fg='#d9d9d9')
    l_frequency.pack(side='left')
    sv_frequency = tk.StringVar(lf_settings, value='10')
    e_frequency = tk.Entry(lf_settings, textvariable=sv_frequency, width=6)
    e_frequency.pack(side='left')
    # Duration entry
    l_duration = tk.Label(lf_settings, text=' Duration (s)', font=text,
                          bg='#292d34', fg='#d9d9d9')
    l_duration.pack(side='right')
    sv_duration = tk.StringVar(lf_settings, value='120')
    e_duration = tk.Entry(lf_settings, textvariable=sv_duration, width=6)
    e_duration.pack(side='right')

    # High workload frame
    lf_high = tk.LabelFrame(root, text='High workload', font=subtitle, bd=3,
                            padx=20, pady=15, bg='#292d34', fg='#ff5444')
    lf_high.pack(fill='both', expand='yes')
    pb_high = ttk.Progressbar(lf_high, orient='horizontal', length=300,
                              mode='determinate')
    button_high = tk.Button(lf_high, text='Run acquisition', font=text,
                            command=lambda: run(e_frequency, e_duration,
                                                'high', pb_high),
                            padx=15, pady=15, bg='#3f3f3f', fg='#d9d9d9')
    button_high.pack()
    pb_high.pack()
    pb_high['maximum'] = 100
    if Path('../data/raw_high.csv').exists():
        pb_high['value'] = 100

    # Medium workload frame
    lf_medium = tk.LabelFrame(root, text='Medium workload', font=subtitle,
                              bd=3, padx=20, pady=15, bg='#292d34',
                              fg='#ff8244')
    lf_medium.pack(fill='both', expand='yes')
    pb_medium = ttk.Progressbar(lf_medium, orient='horizontal', length=300,
                                mode='determinate')
    button_medium = tk.Button(lf_medium, text='Run acquisition', font=text,
                              command=lambda: run(e_frequency, e_duration,
                                                  'medium', pb_medium),
                              padx=15, pady=15, bg='#3f3f3f', fg='#d9d9d9')
    button_medium.pack()
    pb_medium.pack()
    pb_medium['maximum'] = 100
    if Path('../data/raw_medium.csv').exists():
        pb_medium['value'] = 100

    # Low workload frame
    lf_low = tk.LabelFrame(root, text='Low workload', font=subtitle, bd=3,
                           padx=20, pady=15, bg='#292d34', fg='#ffd944')
    lf_low.pack(fill='both', expand='yes')
    pb_low = ttk.Progressbar(lf_low, orient='horizontal', length=300,
                             mode='determinate')
    button_low = tk.Button(lf_low, text='Run acquisition', font=text,
                           command=lambda: run(e_frequency, e_duration,
                                               'low', pb_low),
                           padx=15, pady=15, bg='#3f3f3f', fg='#d9d9d9')
    button_low.pack()
    pb_low.pack()
    pb_low['maximum'] = 100
    if Path('../data/raw_low.csv').exists():
        pb_low['value'] = 100

    # Processing button
    f_process = tk.Frame(root, bd=3, padx=20, pady=5, bg='#292d34')
    f_process.pack(fill='both', expand='yes')
    button_launch = tk.Button(f_process, text='Process', font=text,
                              command=lambda: process(conditions, root),
                              padx=15, pady=5, bg='#3f3f3f', fg='#d9d9d9')
    button_launch.pack()

    # Display window until it is closed
    root.mainloop()


if __name__ == '__main__':
    main()
