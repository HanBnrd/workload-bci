# Emulate sending data (ASCII) from OctaMon on OxySoft
# © Johann Benerradi

import numpy as np
import socket
import time


HOST = "127.0.0.1"
PORT = 7777


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print(f"Connected by {addr}")
        i = 0
        while True:
            data = np.random.rand(16)
            string_data = f"{i}, {', '.join(str(x) for x in data)}"
            string_data = f"b'{string_data}\n'"
            try:
                conn.send(string_data.encode())
            except ConnectionResetError:
                conn.close()
                print(f"Connection closed")
                break
            i += 1
            time.sleep(0.5)
