# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module provides tools for the GUI
"""

from threading import Thread
import webbrowser
import time
import platform
import os


class Progressbar(Thread):

    def __init__(self, duration, progressbar, baseline):
        Thread.__init__(self)
        self.duration = duration
        self.baseline = baseline
        self.progressbar = progressbar

    def run(self):
        self.progressbar['value'] = 0
        if self.baseline > 0:
            for _ in range(self.baseline):
                time.sleep(1)
                self.progressbar['value'] += 100/self.baseline
            self.progressbar['value'] = 0
        for _ in range(self.duration):
            time.sleep(1)
            self.progressbar['value'] += 100/self.duration


class Launcher(Thread):

    def __init__(self, condition_name, baseline):
        Thread.__init__(self)
        self.baseline = baseline
        self.operating_system = platform.system()
        self.baseline_url = os.path.abspath('../img/baseline.png')
        if condition_name == 'high':
            self.game_url = 'http://arithmetic.zetamac.com/game?key=e3868c6e'
        if condition_name == 'medium':
            self.game_url = 'http://arithmetic.zetamac.com/game?key=a644c714'
        if condition_name == 'low':
            self.game_url = 'http://arithmetic.zetamac.com/game?key=88d54c5f'
        if condition_name == 'acquisition':
            self.game_url = 'https://start.duckduckgo.com/'

    def run(self):
        if self.operating_system == 'Windows':
            firefox_path = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
            webbrowser.register('firefox', None,
                                webbrowser.BackgroundBrowser(firefox_path))
        if self.baseline > 0:
            webbrowser.get('firefox').open_new_tab(self.baseline_url)
            time.sleep(self.baseline)
        webbrowser.get('firefox').open_new_tab(self.game_url)
