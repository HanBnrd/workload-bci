# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__title__ = 'Workload BCI'
__version__ = '0.1'
__author__ = 'Johann Benerradi'
__copyright__ = 'Copyright (C)  Johann Benerradi'
__license__ = 'GNU GPL 3.0'

from .export import Export
from .tools import Progressbar
from .tools import Launcher
from .process import Process
