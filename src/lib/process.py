# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module processes the exported data
"""

import numpy as np
import math


class Process():

    def get_extremums(self):
        """
        Get minimums and maximums
        """
        # Open CBSI files
        cbsi_high = open('../data/cbsi_high.csv', 'r', encoding='utf8')
        cbsi_medium = open('../data/cbsi_medium.csv', 'r', encoding='utf8')
        cbsi_low = open('../data/cbsi_low.csv', 'r', encoding='utf8')
        files = [cbsi_high, cbsi_medium, cbsi_low]

        # Prepare lists
        max_x0 = [-math.inf for i in range(8)]
        min_x0 = [math.inf for i in range(8)]

        # Search for max and min in all the files
        for file in files:
            file.readline()  # handle headers
            for line in file:
                x0 = line.split(',')[1:]
                x0 = [float(i) for i in x0]
                if not np.any(np.isnan(x0)) and not np.any(np.isinf(x0)):
                    max_x0 = np.maximum(max_x0, x0)
                    min_x0 = np.minimum(min_x0, x0)

        # Print in console
        print('max: ' + str(max_x0))
        print('min: ' + str(min_x0))

        # Open the file to store the extremums
        file_extremums = open('../data/extremums.csv', 'w', encoding='utf8')

        # Write maximum
        file_extremums.write('max')
        for channel in max_x0:
            file_extremums.write(',' + str(channel))
        file_extremums.write('\n')

        # Write minimum
        file_extremums.write('min')
        for channel in min_x0:
            file_extremums.write(',' + str(channel))
        file_extremums.write('\n')

        # Close files
        cbsi_high.close()
        cbsi_medium.close()
        cbsi_low.close()
        file_extremums.close()

    def normalize(self, condition_name):
        """
        Compute normalization between 0 and 1
        """
        # Open files
        filename_cbsi = '../data/cbsi_' + condition_name + '.csv'
        file_cbsi = open(filename_cbsi, 'r', encoding='utf8')
        file_extremums = open('../data/extremums.csv', 'r', encoding='utf8')
        filename_normalized = '../data/normalized_' + condition_name + '.csv'
        file_normalized = open(filename_normalized, 'w', encoding='utf8')

        # Get max, min and range
        max_x0 = file_extremums.readline().split(',')[1:]
        max_x0 = np.array([float(i) for i in max_x0])
        min_x0 = file_extremums.readline().split(',')[1:]
        min_x0 = np.array([float(i) for i in min_x0])
        range_x0 = max_x0 - min_x0

        # Handle headers
        headers = file_cbsi.readline()
        file_normalized.write(headers)

        for line in file_cbsi:
            # Normalize the CBSI data
            sample = line.split(',')[0]
            x0 = line.split(',')[1:]
            x0 = np.array([float(i) for i in x0])
            normalized_x0 = (x0 - min_x0) / range_x0

            # Write in the normalized file
            file_normalized.write(sample)
            for channel in normalized_x0:
                file_normalized.write(',' + str(round(channel, 2)))
            file_normalized.write('\n')
        file_cbsi.close()
        file_extremums.close()
        file_normalized.close()

    def average(self, condition_name):
        """
        Compute average by sides
        """
        # Open files
        filename_cbsi = '../data/cbsi_' + condition_name + '.csv'
        file_cbsi = open(filename_cbsi, 'r', encoding='utf8')
        filename_average = '../data/average_' + condition_name + '.csv'
        file_average = open(filename_average, 'w', encoding='utf8')

        # Handle headers
        file_cbsi.readline()
        headers = 'Sample,Rx1(Right),Rx2(Left)\n'
        file_average.write(headers)

        # Get coordinates and average
        for line in file_cbsi:
            sample = line.split(',')[0]
            x0_rx1 = line.split(',')[1:5]
            x0_rx1 = [float(i) for i in x0_rx1]
            x0_rx1 = np.mean(x0_rx1)
            x0_rx2 = line.split(',')[5:]
            x0_rx2 = [float(i) for i in x0_rx2]
            x0_rx2 = np.mean(x0_rx2)
            file_average.write(sample + ',' + str(round(x0_rx1, 2)) + ',' +
                               str(round(x0_rx2, 2)) + '\n')
        file_cbsi.close()
        file_average.close()

    def learn_svm(self):
        pass
