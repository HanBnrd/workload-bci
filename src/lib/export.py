# -*- coding: utf-8 -*-

# Workload BCI
# Copyright (C)  Johann Benerradi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This module exports the data in real-time from Oxysoft
"""

from threading import Thread
from socket import socket
import numpy as np


class Export(Thread):

    TCP_IP = '127.0.0.1'
    TCP_PORT = 7777
    BUFFER_SIZE = 500  # must be more than the size of a line
    WINDOW_SIZE = 30  # window for CBSI (sec), at least 30 (Cui et al., 2009)

    def __init__(self, frequency, duration, condition_name):
        Thread.__init__(self)
        self.frequency = frequency
        self.duration = duration
        self.filename_raw = '../data/raw_' + condition_name + '.csv'
        self.filename_cbsi = '../data/cbsi_' + condition_name + '.csv'

    def cbsi(self, x, y, buffer_x, buffer_y):
        """
        Compute CBSI
        """
        alpha = np.std(buffer_x, axis=0) / np.std(buffer_y, axis=0)
        x0 = (x - alpha*y) / 2
        return x0

    def run(self):
        print('Data acquisition...')
        s = socket()
        s.connect((self.TCP_IP, self.TCP_PORT))

        # Open files to save the data
        file_raw = open(self.filename_raw, 'w', encoding='utf8')
        file_cbsi = open(self.filename_cbsi, 'w', encoding='utf8')

        # Handle headers for the raw data
        data = s.recv(self.BUFFER_SIZE)  # recieve first buffer
        if data:
            # Format the data
            line = str(data)
            line = line.split('b\'')[1]
            line = line.split('\\n')[0]
            # Write in the raw file
            file_raw.write(line + '\n')

        # Write headers in the CBSI file
        headers = 'Sample,Rx1-Tx1,Rx1-Tx2,Rx1-Tx3,Rx1-Tx4,' \
                  'Rx2-Tx5,Rx2-Tx6,Rx2-Tx7,Rx2-Tx8\n'
        file_cbsi.write(headers)

        # Prepare lists
        buffer_x = []  # most recent measured oxys
        buffer_y = []  # most recent measured deoxys

        for _ in range(self.duration*self.frequency):
            # Recieve one line
            data = s.recv(self.BUFFER_SIZE)

            # End the loop if nothing is recieved
            if not data:
                break

            # Format the data
            line = str(data)
            line = line.split('b\'')[1]
            line = line.split('\\n\'')[0]

            # Write in the raw file
            file_raw.write(line + '\n')

            # Get coordinates
            sample_number = line.split(', ')[0]
            x = line.split(', ')[1::2]
            x = [float(i) for i in x]
            y = line.split(', ')[2::2]
            y = [float(i) for i in y]
            buffer_x.append(x)
            buffer_y.append(y)

            if len(buffer_x) > self.WINDOW_SIZE*self.frequency:
                buffer_x.pop(0)
                buffer_y.pop(0)
                # Compute CBSI
                x0 = self.cbsi(x, y, buffer_x, buffer_y)

                # Write in the CBSI file
                file_cbsi.write(sample_number)
                for channel in x0:
                    file_cbsi.write(',' + str(round(channel, 2)))
                file_cbsi.write('\n')

        # Close files
        file_raw.close()
        file_cbsi.close()

        print('Done.')
