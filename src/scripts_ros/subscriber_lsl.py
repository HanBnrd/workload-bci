# ROS subscriber for LSL data from OxySoft
# © Johann Benerradi

import rospy
from std_msgs.msg import Float64MultiArray


def callback(data):
    rospy.loginfo(f"{rospy.get_caller_id()}: received {data.data}")


def listener():
    rospy.init_node("listener", anonymous=True)
    rospy.Subscriber("lsl_data", Float64MultiArray, callback)
    rospy.spin()  # keep python from exiting until this node is stopped


if __name__ == "__main__":
    listener()
