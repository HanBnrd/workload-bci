# Emulate sending OD and ADC data from OctaMon on OxySoft via LSL
# © Johann Benerradi
# 1) Run this script to emulate a random OctaMon LSL feed for testing purposes

import pandas as pd
import random
import time

from pylsl import StreamInfo, StreamOutlet, local_clock


NAME = "OxySoft"
STREAM_TYPE = "NIRS"
SFREQ = 10  # Hz


def main(device="octamon"):
    """
    Emulate sending OD and ADC data from OctaMon on OxySoft via LSL.

    Parameters
    ----------
    device : string
        Device to emulate, can be `'octamon'` (default) or `'brite'`.
    """
    # Load sample data
    data = pd.read_csv(f"../../data/sample_data_{device}.csv")
    n_channels = len(data.columns[1:])

    # Create stream info
    info = StreamInfo(NAME, STREAM_TYPE, n_channels, SFREQ, source_id=NAME)
    # source_id being the device serial number or unique stream identifier
    # (could also omit it but interrupted connections wouldn't auto-recover)

    # Add channel info from sample file to stream info
    chs = info.desc().append_child("channels")
    for header in data.columns[1:]:
        label = header[:header.find(" (")]
        ch_type = header[header.find("(")+1:header.find(")")]
        ch = chs.append_child("channel")
        ch.append_child_value("label", label)
        ch.append_child_value("type", ch_type)

    # Data stream
    outlet = StreamOutlet(info)

    start_time = local_clock()  # in sec, from when local machine was started
    sent_samples = 0
    print("Sending data...")
    while True:
        # Calculate required samples based on clock and sfreq
        elapsed_time = local_clock() - start_time
        required_samples = int(SFREQ * elapsed_time) - sent_samples

        for _ in range(required_samples):
            # Send random sample
            random_line_idx = random.randint(0, data.shape[0]-1)
            sample = list(data.iloc[random_line_idx][1:])
            outlet.push_sample(sample)
        sent_samples += required_samples
        time.sleep(0.01)  # wait before trying again


if __name__ == "__main__":
    # main("octamon")
    main("brite")
