# Read BCI class via LSL
# © Johann Benerradi
# 1) Run oxysoft_live_processing.py
# 2) Run this script

from pylsl import StreamInlet, resolve_stream


def main():
    # Event stream
    bci_stream = resolve_stream("name", "BCI Class")[0]
    bci_inlet = StreamInlet(bci_stream)

    print("Reading data...")
    while True:
        # Get new sample
        sample, timestamp = bci_inlet.pull_sample()
        print(f"Time: {timestamp}, Class: {sample}")


if __name__ == "__main__":
    main()
