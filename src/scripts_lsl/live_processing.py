# Read multi-channel OD and ADC data from OxySoft via LSL and process it
# © Johann Benerradi
# 1) Setup LSL on OxySoft
# 2) Run this script

import mne
import nirsimple as ns
import numpy as np
import pandas as pd
import re
import warnings

from mne.preprocessing.nirs import temporal_derivative_distribution_repair
from pylsl import StreamInfo, StreamInlet, StreamOutlet, resolve_stream
from scipy.stats import linregress


GYRO = ['HEADING', 'ROLL', 'PITCH']
mne.set_log_level(verbose=False)


def main(device="octamon"):
    """
    Read multi-channel OD and ADC data from OxySoft via LSL and process it.

    Parameters
    ----------
    device : string
        Device to emulate, can be `'octamon'` (default) or `'brite'`.
    """
    # Data stream
    data_stream = resolve_stream("name", "OxySoft")[0]
    data_inlet = StreamInlet(data_stream)
    info = data_inlet.info()
    name, sfreq, stream_type = info.name(), info.nominal_srate(), info.type()
    print(f"Reading {name} stream of {sfreq} Hz {stream_type}...")
    all_chs = []
    all_ch_wls = []
    ch = info.desc().child("channels").child("channel")
    for _ in range(info.channel_count()):
        label = ch.child_value("label")
        if re.match(r"\[\d*\] .* \[\d*nm\]", label):
            all_chs.append(label.split("] ")[1].split(" [")[0])
            all_ch_wls.append(label.split(" [")[1].split("nm]")[0])
        else:
            all_chs.append(label)
            all_ch_wls.append(None)
        ch = ch.next_sibling()

    keep = pd.read_csv(f"../../info/optodes_{device}.csv")
    ch_rls = keep.columns
    ch_sds = keep.values[0]
    indices_keep = [all_chs.index(ch_keep) for ch_keep in ch_rls]
    ch_names = [ch.split(' ')[0] for ch in ch_sds]
    ch_wls = [all_ch_wls[i] for i in indices_keep]
    ch_dpfs = [6.0 for _ in ch_sds]
    ch_distances = keep.values[1].astype(float)

    indices_gyro = [all_chs.index(ch_gyro) for ch_gyro in GYRO
                    if ch_gyro in all_chs]

    # BCI stream
    bci_info = StreamInfo("BCI Class", "Markers", 1, 0, "string", "BCI Class")
    bci_outlet = StreamOutlet(bci_info)

    od_buffer = np.empty((len(indices_keep), 0))
    gyro_buffer = np.empty((len(indices_gyro), 0))
    while True:
        # Get new sample
        sample, timestamp = data_inlet.pull_sample()
        sample_array = np.array(sample)
        od = sample_array[indices_keep]
        od = od[:, np.newaxis]
        gyro = sample_array[indices_gyro]
        gyro = gyro[:, np.newaxis]

        # Append new sample
        od_buffer = np.append(od_buffer, od, axis=1)
        gyro_buffer = np.append(gyro_buffer, gyro, axis=1)

        # Wait until buffer has 10 sec of data
        if od_buffer.shape[1] < (10*sfreq):
            continue

        # Preprocessing
        warnings.simplefilter("ignore")
        dod = ns.od_to_od_changes(od_buffer)
        mbll_data = ns.mbll(dod, ch_names, ch_wls, ch_dpfs, ch_distances, "cm")
        dc, ch_names, ch_types = mbll_data
        warnings.resetwarnings()

        # Loading with MNE
        mne_ch_names = [f"{ch} {ch_types[i]}" for i, ch in enumerate(ch_names)]
        info = mne.create_info(ch_names=mne_ch_names, sfreq=sfreq,
                               ch_types=ch_types)
        raw = mne.io.RawArray(dc, info)

        # TDDR
        raw = temporal_derivative_distribution_repair(raw)

        # Bandpass filtering
        iir_params = dict(order=4, ftype="butter", output="sos")
        raw.filter(0.01, 0.5, method="iir", iir_params=iir_params)

        # Feature extraction
        raw_hbo_mean = raw.get_data(picks="hbo", units="uM").mean(axis=0)
        slope = linregress(raw.times, raw_hbo_mean).slope

        # Process gyro data
        if indices_gyro:
            gyro_std = gyro_buffer.std()
        else:
            gyro_std = None

        # Print processed data
        print(f"fNIRS: {slope} | Gyro: {gyro_std}")

        # Send BCI class
        if slope >= 0:
            bci_outlet.push_sample(["Increase"])
        else:
            bci_outlet.push_sample(["Decrease"])

        # Pop oldest sample
        od_buffer = np.delete(od_buffer, 0, axis=1)
        gyro_buffer = np.delete(gyro_buffer, 0, axis=1)


if __name__ == "__main__":
    # main("octamon")
    main("brite")
