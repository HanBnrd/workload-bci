# Read events from OxySoft via LSL
# © Johann Benerradi
# 1) Setup LSL on OxySoft
# 2) Run this script

from pylsl import StreamInlet, resolve_stream


def main():
    # Prep table
    event_file = open("../../data/event_file.csv", "w")
    headers_string = "\"Timestamp\",\"Event\""
    event_file.write(headers_string + "\n")

    # Event stream
    event_stream = resolve_stream("name", "OxySoft Event")[0]
    event_inlet = StreamInlet(event_stream)

    print("Reading data...")
    for _ in range(9):
        # Get new sample
        sample, timestamp = event_inlet.pull_sample()
        event_string = f"{timestamp},"
        event_string += ",".join(str(x) for x in sample)
        event_file.write(event_string + "\n")

    event_file.close()


if __name__ == "__main__":
    main()
