# Read multi-channel OD and ADC data from OxySoft via LSL
# © Johann Benerradi
# 1) Setup LSL on OxySoft
# 2) Run this script

from pylsl import StreamInlet, resolve_stream


def main():
    # Prep table
    data_file = open("../../data/data_file.csv", "w")
    headers_string = "\"Timestamp\","

    # Data stream
    data_stream = resolve_stream("name", "OxySoft")[0]
    data_inlet = StreamInlet(data_stream)
    info = data_inlet.info()
    name, sfreq, stream_type = info.name(), info.nominal_srate(), info.type()
    print(f"Source ID: {info.source_id()}")  # 'OxySoft'
    print(f"Reading {name} stream of {sfreq} Hz {stream_type}...")
    chs = []
    ch = info.desc().child("channels").child("channel")
    for _ in range(info.channel_count()):
        chs.append(f"{ch.child_value('label')} ({ch.child_value('type')})")
        ch = ch.next_sibling()
    headers_string += ",".join(f"\"{str(x)}\"" for x in chs)
    data_file.write(headers_string + "\n")

    for _ in range(100):
        # Get new sample
        sample, timestamp = data_inlet.pull_sample()
        print(timestamp, sample)
        data_string = f"{timestamp},"
        data_string += ",".join(str(x) for x in sample)
        data_file.write(data_string + "\n")

    data_file.close()


if __name__ == "__main__":
    main()
