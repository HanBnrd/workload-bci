# Infer workload from live EEG using a trained classifier
# © Johann Benerradi

import mne
import numpy as np
import pickle
from scipy import signal

from pylsl import StreamInfo, StreamInlet, StreamOutlet, resolve_stream


FEATURE = 'A'  # 'A', 'BTR' 'ABTR' or '110'
mne.set_log_level(verbose=False)


def main():
    # Data stream
    data_stream = resolve_stream("name", "StreamName")[0]
    data_inlet = StreamInlet(data_stream)
    info = data_inlet.info()
    name, sfreq, stream_type = info.name(), info.nominal_srate(), info.type()
    print(f"Reading {name} stream of {sfreq} Hz {stream_type}...")

    # Select channels
    indices_keep = [0, 1, 2, 3, 4, 5, 6, 7]

    # BCI stream
    bci_info = StreamInfo("BCI", "Markers", 1, 0, "string", "WKLD")
    bci_outlet = StreamOutlet(bci_info)

    # Load classifier
    with open('./clf.pickle', 'rb') as file_reader:
        clf = pickle.load(file_reader)
    print(f"Loaded classifier: {clf}")

    #############
    # MAIN LOOP #
    #############
    raw_buffer = np.empty((len(indices_keep), 0))
    while True:
        # Get new sample
        sample, timestamp = data_inlet.pull_sample()
        sample_array = np.array(sample)
        raw_sample = sample_array[indices_keep]
        raw_sample = raw_sample[:, np.newaxis]

        # Append new sample
        raw_buffer = np.append(raw_buffer, raw_sample, axis=1)

        # Wait until buffer has 5 sec of data
        if raw_buffer.shape[1] < (5*sfreq):
            continue

        # Bandpass filtering
        raw_data = mne.filter.filter_data(
            raw_buffer, sfreq, 1, 40, method='iir',
            iir_params={'ftype': 'butter', 'order': 4})

        # Feature extraction
        f, psd = signal.welch(raw_data, 250)
        low_a = np.argmax(f > 8.0)
        up_a = np.argmin(f < 12.0)
        low_b = np.argmax(f > 13.0)
        up_b = np.argmin(f < 30.0)
        low_t = np.argmax(f > 4.0)
        up_t = np.argmin(f < 7.0)
        low_110 = np.argmax(f > 1.0)
        up_110 = np.argmin(f < 10.0)
        f_alpha, psd_alpha = f[low_a:up_a], psd[:, low_a:up_a]
        f_beta, psd_beta = f[low_b:up_b], psd[:, low_b:up_b]
        f_theta, psd_theta = f[low_t:up_t], psd[:, low_t:up_t]
        f_110, psd_110 = f[low_110:up_110], psd[:, low_110:up_110]
        if FEATURE == 'A':
            psd_feature = psd_alpha.mean(axis=-1)
        elif FEATURE == 'BTR':
            psd_feature = psd_beta.mean(axis=-1) / psd_theta.mean(axis=-1)
        elif FEATURE == 'ABTR':
            psd_alpha_theta = psd_alpha.mean(axis=-1) + psd_theta.mean(axis=-1)
            psd_feature = psd_beta.mean(axis=-1) / psd_alpha_theta
        elif FEATURE == '110':
            psd_feature = psd_110.mean(axis=-1)

        # Model prediction
        prediction = clf.predict(psd_feature[np.newaxis, :])[0]
        proba = clf.predict_proba(psd_feature[np.newaxis, :])[0]
        proba_low, proba_high = proba
        print(f"Prediction: {prediction} "
              f"(0 (low): {proba_low}, 1 (high):{proba_high})")

        # Send classifier prediction
        if prediction == 0:
            bci_outlet.push_sample(["Low"])
        elif prediction == 1:
            bci_outlet.push_sample(["High"])

        # Pop oldest sample
        raw_buffer = np.delete(raw_buffer, 0, axis=1)


if __name__ == "__main__":
    main()
