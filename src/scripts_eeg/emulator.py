# Emulate sending EEG data via LSL
# © Johann Benerradi

import pandas as pd
import random
import time

from pylsl import StreamInfo, StreamOutlet, local_clock


SFREQ = 250  # Hz


def main():
    """
    Emulate sending EEG data via LSL.
    """
    # Load sample data
    data = pd.read_csv(f"../../data/eeg.csv")
    n_channels = len(data.columns[:])

    # Create stream info
    info = StreamInfo('StreamName', 'EEG', n_channels, SFREQ, source_id='001')
    # source_id being the device serial number or unique stream identifier
    # (could also omit it but interrupted connections wouldn't auto-recover)

    # Add channel info from sample file to stream info
    chs = info.desc().append_child("channels")
    for header in data.columns[:]:
        label = header
        ch_type = "AUX"
        ch = chs.append_child("channel")
        ch.append_child_value("label", label)
        ch.append_child_value("type", ch_type)

    # Data stream
    outlet = StreamOutlet(info)

    start_time = local_clock()  # in sec, from when local machine was started
    sent_samples = 0
    print("Sending data...")
    while True:
        # Calculate required samples based on clock and sfreq
        elapsed_time = local_clock() - start_time
        required_samples = int(SFREQ * elapsed_time) - sent_samples

        for _ in range(required_samples):
            # Send random sample
            random_line_idx = random.randint(0, data.shape[0]-1)
            sample = list(data.iloc[random_line_idx][:])
            outlet.push_sample(sample)
        sent_samples += required_samples
        time.sleep(1/SFREQ)  # wait before trying again


if __name__ == "__main__":
    main()
